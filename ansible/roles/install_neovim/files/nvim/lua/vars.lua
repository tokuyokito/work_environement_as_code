local module = {}
-- List of possible extension at :
-- https://github.com/nvim-treesitter/nvim-treesitter/blob/master/README.md
module.treesitter_ensure_installed = { "python", "yaml", "json", "dockerfile", "markdown"}

-- List of supported lsp_servers at :
-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
-- Or use Mason to list available servers 
-- Activate server
module.lsp_servers = {'rust_analyzer', 'pyright', 'bashls'}

return module
