vim.keymap.set('n', '<F1>', ':NvimTreeToggle<CR>', {})
vim.keymap.set('n', '<F2>', ':NvimTreeFocus<CR>', {})
vim.keymap.set('n', '<F3>', ':NvimTreeFindFile<CR>', {}) 
vim.keymap.set('n', '<F4>', ':NvimTreeCollapse<CR>', {}) 
