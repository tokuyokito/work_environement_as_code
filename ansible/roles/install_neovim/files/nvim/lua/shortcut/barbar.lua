vim.api.nvim_set_keymap('n', '<tab>', ":BufferNext<CR>", {})
vim.api.nvim_set_keymap('n', '<S-Tab>', ":BufferPrevious<CR>", {})
vim.api.nvim_set_keymap('n', '<S-q>', ":BufferClose<CR>", {})
vim.api.nvim_set_keymap('n', '<S-w>', ":BufferCloseAllButCurrent<CR>", {})
vim.api.nvim_set_keymap('n', '<S-x>', ":BufferGoto ", {})

