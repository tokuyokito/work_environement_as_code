-- Plugins manage by packer
-- https://github.com/wbthomason/packer.nvim

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  
  use {
    'nvim-telescope/telescope-file-browser.nvim', -- https://github.com/nvim-telescope/telescope-file-browser.nvim -- File browser
    'nvim-lualine/lualine.nvim', -- https://github.com/nvim-lualine/lualine.nvim -- Airline
    'BurntSushi/ripgrep',        -- https://github.com/BurntSushi/ripgrep        -- Recursively searches
    'sharkdp/fd',                -- https://github.com/sharkdp/fd                -- Alternative to find
    'nvim-lua/plenary.nvim',     -- https://github.com/nvim-lua/plenary.nvim     -- Module for asynchronous programming
    'nvim-treesitter/nvim-treesitter', -- https://github.com/nvim-treesitter/nvim-treesitter -- highlighting 
    'romgrk/barbar.nvim',              -- https://github.com/romgrk/barbar.nvim              -- Top bar for table
    'kyazdani42/nvim-web-devicons',    -- https://github.com/nvim-tree/nvim-web-devicons     -- Icones for style        
  }

  use 'nvim-tree/nvim-tree.lua' -- https://github.com/nvim-tree/nvim-tree.lua -- SideBar 

  use 'neovim/nvim-lspconfig' -- Collection of configurations for built-in LSP client
  use 'hrsh7th/nvim-cmp' -- Autocompletion plugin
  use 'hrsh7th/cmp-nvim-lsp' -- LSP source for nvim-cmp
  use 'saadparwaiz1/cmp_luasnip' -- Snippets source for nvim-cmp
  use 'L3MON4D3/LuaSnip' -- Snippets plugin
  use 'williamboman/mason.nvim' -- LSP servers, DAP servers, linters, and formatters manager
  use 'williamboman/mason-lspconfig.nvim' -- Bridges mason and lspconfig 
  use "rafamadriz/friendly-snippets"-- https://github.com/L3MON4D3/LuaSnip -- Snippets collection for a set of different programming languages

  -- Colorshemes
  -- use 'Shatur/neovim-ayu' -- https://github.com/Shatur/neovim-ayu -- Colorsheme
  -- https://github.com/sainnhe/everforest
  use 'sainnhe/everforest'

  -- https://github.com/nvim-telescope/telescope-file-browser.nvim
  -- Require package ripgrep and rust-fd-find
  use { 
    'nvim-telescope/telescope.nvim', 
    require = {{'nvim-lua/plenary.nvim'}}
  }

  -- https://github.com/windwp/nvim-autopairs
  -- autopair plugin for Neovim that supports multiple characters
  use 'windwp/nvim-autopairs'
  use 'mzlogin/vim-markdown-toc'

end)
