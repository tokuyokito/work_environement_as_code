vim.g.loaded_netrw = 1                -- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrwPlugin = 1          -- See above 
vim.opt.relativenumber = true         -- Show number in relative mode
vim.opt.autoindent     = true         -- Smart indentation
vim.opt.encoding       = "utf-8"      --
vim.opt.fileencoding   = "utf-8"      --
vim.opt.scrolloff      = 10           -- Vertically align
vim.opt.syntax         = "on"         -- Activate color
vim.opt.shiftwidth     = 4            -- Use 4 spaces instead of tabs
vim.opt.mouse          = "a"          -- Allow mouse usage 
vim.opt.cursorline     = true         -- Display a box for the line where the cursor is
vim.opt.termguicolors  = true         -- Enable 24 bits RGB 
vim.opt.wildmenu       = true         -- Vim normal mode completion menu
vim.opt.completeopt    = "menuone,noselect,noinsert,preview" -- Comesctic configuration for wildmenu
vim.opt.updatetime     = 750 -- Vim auto Refresh in milliseconds


